import React,{Component} from 'react';
import {View,Text}from 'react-native';
import { connect} from 'react-redux';
import {emailChange ,passwordChange,loginUser} from '../actions';
import{Card,CardSection,Input,Button,Spinner}from './common';


class LoginForm extends Component{

    

    onEmailChange(text){
        this.props.emailChange(text);   
    }
    onChangePassword(text){
        this.props.passwordChange(text);   
    }
    onButtonPress(){
        const {email,password}= this.props;

        this.props.loginUser({email,password})
    }



    renderButton(){
            if(this.props.loading)
            {
                return<Spinner size = "large"/>
            }
            else{
                return<Button onPress = {this.onButtonPress.bind(this)}>LOG IN </Button>
            }
    }

    render(){
        
        return(
            <Card>
                <CardSection>
                    <Input
                    label ='Email'
                    placeholder = 'email@gmail.com' 
                    onChangeText = {this.onEmailChange.bind(this)}
                    value ={this.props.email}
                    />
                    </CardSection>
                         <CardSection>
                         <Input
                    label ='Password'
                    placeholder = 'password'
                    secureTextEntry 
                    onChangeText ={this.onChangePassword.bind(this)}
                    value ={this.props.password}
                    />
                            </CardSection>
                            
                             
                                <CardSection>
                                   {this.renderButton()}
                                     </CardSection>

                </Card>
        );
    }

}

const styles = {
    errorTextStyle:{
        fontSize:20,
        alignSelf:'center',
        color:'red'
    }
}

const mapStateToProps = ({auth}) =>{
    const {email,password,error,loading}= auth;
    return{
       email,password,error,loading
    };
};
export default connect(mapStateToProps,{emailChange,passwordChange,loginUser})(LoginForm);
