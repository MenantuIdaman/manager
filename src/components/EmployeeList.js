import React,{Component} from 'react';
import {ListView,TouchableOpacity,Text} from 'react-native';
import ListItem from './ListItem'
import { connect} from 'react-redux';
import {NavigationActions}from 'react-navigation';
import {employeeFetch,NavigateAdd}from'../actions';

import firebase from 'firebase';
import _ from 'lodash'; 



class EmployeeList extends Component{


// constructor(props){
//     super(props);
//     const{NavigateAdd} = this.props;
//     this.state ={
//         NavigateAdd,
//     };
//     this.onNavigateAdd = this.onNavigateAdd.bind(this);
// }

    

// onNavigateAdd(){
//     this.setState(this.props.NavigateAdd())
// }
    
    
        
componentWillMount(){
    this.props.employeeFetch();
    this.createDataSource(this.props);
    console.log("props:" + JSON.stringify(this.props));
}

componentWillReceiveProps(nextProps){
this.createDataSource(nextProps);
}

createDataSource({employees}){
    
    const ds = new ListView.DataSource({
        rowHasChanged:(r1,r2)=>r1!=r2
    })

    this.dataSource = ds.cloneWithRows(employees);
}

renderRow(employee)
{
    return<ListItem employee ={employee}/>
}

static navigationOptions =({navigation})=> ({
    title: 'Employee List',
    headerLeft:null,
    headerRight:(
        <TouchableOpacity
        onPress={()=>navigation.navigate('EmployeeCreate')}
        style ={{paddingRight:20}}
        >
                <Text>
                    Add
                    </Text>
            </TouchableOpacity>
    )
  });

render()
{

 
    return(
        <ListView
            enableEmptySections
            dataSource={this.dataSource}
            renderRow ={this.renderRow}
        />
    );
};


}


const mapStateToProps = state =>{
    const employees = _.map(state.employees,(val,uid)=>{
    return{...val ,uid};
    });
    return {employees};
};

export default connect(mapStateToProps,{employeeFetch,NavigateAdd})(EmployeeList);