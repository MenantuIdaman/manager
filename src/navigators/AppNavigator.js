import React ,{Component}from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {BackHandler,Text,BackAndroid,Alert,Platform,TouchableOpacity} from 'react-native';
import { addNavigationHelpers, StackNavigator, NavigationActions  } from 'react-navigation';

import LoginForm from '../components/LoginForm';
import EmployeeList from '../components/EmployeeList';
import EmployeeCreate from '../components/EmpolyeeCreate';
import EmployeeEdit from '../components/EmployeeEdit';



export const AppNavigator = StackNavigator({
  Login: { screen: LoginForm ,navigationOptions:{
    title:'Login',headerTitleStyle:{alignSelf: 'center'},
  },
},

  Main: { 
    screen: EmployeeList,
    navigationOptions:{
     
    },
  //   navigationOptions: ({ navigation }) => ({
  //     headerRight:(
  //         <TouchableOpacity
  //         style ={{paddingRight:20}}
  //         onPress ={()=>navigation.navigate('EmployeeCreate')}>
  //             <Text>Add</Text>
  //             </TouchableOpacity>
  //     ),
  //     title:'Employee List',
  //     headerTitleStyle:{
  //       alignSelf:'center',
  //       paddingLeft:50
  //     },
  //     headerLeft:null
  // }) 

},
  EmployeeCreate:{
    screen:EmployeeCreate,
    navigationOptions:{
    title:'Create Employee',
    
  }
  },
  EmployeeEdit:{
    screen:EmployeeEdit,
    navigationOptions:{
      title:'Edit Employee',
      
    }
  }
},{
  initialRouteName:'Login',
  headerMode:'screen'

});

// Prevents double taps navigating twice
const navigateOnce = (getStateForAction) => (action, state) => {
  const { type, routeName } = action;
  return (
      state &&
      type === NavigationActions.NAVIGATE &&
      routeName === state.routes[state.routes.length - 1].routeName
  ) ? state : getStateForAction(action, state);
};
AppNavigator.router.getStateForAction = navigateOnce(AppNavigator.router.getStateForAction);

class AppNavigation extends Component{
    
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
  }
  onBackPress = () => {
    const { dispatch, nav } = this.props;
     const routeName = nav.routes[nav.index].routeName;

    if (routeName === 'Login' ) {
      return false;
    }

    if (routeName === 'Main') {
      BackHandler.exitApp();
    }

    if (nav && nav.routes && nav.routes.length > 0) {
      dispatch(NavigationActions.back());
      return true;
    }
    return false;
    
    return true;
  };


  render()
  {
    const { dispatch, nav } = this.props;

    return(
      <AppNavigator navigation = {addNavigationHelpers({dispatch,state:nav})}/>
      
    );
  };
};

const mapStateToProps = state =>({
  nav:state.nav,
});

export default connect(mapStateToProps)(AppNavigation);

