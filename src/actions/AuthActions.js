import {
    EMAIL_CHANGED, 
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILED,
    LOGIN_USER
} from './types';
import firebase from 'firebase';
import { NavigationActions } from 'react-navigation';

export const emailChange = (text)=>{
    return{
        type:EMAIL_CHANGED,
        payload:text
    };
}

export const passwordChange =(text)=>{
    return{
        type:PASSWORD_CHANGED,
        payload:text
    };
};


export const loginUser = ({email,password})=>(dispatch)=>{
    dispatch({
        type:LOGIN_USER
    });
    firebase.auth().signInWithEmailAndPassword(email,password).then(user =>loginUserSuccess(dispatch,user)).catch(()=>{
    // firebase.auth().createUserWithEmailAndPassword(email,password).then(user=>loginUserSuccess(dispatch,user))
   loginUserFailed(dispatch)
        });
    };


const loginUserSuccess =(dispatch,user)=>{
    dispatch({
        type:LOGIN_USER_SUCCESS,
        payload:user
    });
   dispatch(NavigationActions.navigate({ routeName:'Main'}))

};

const loginUserFailed = (dispatch)=>{
    dispatch({
        type:LOGIN_USER_FAILED
    });
};

