/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  View
} from 'react-native';
import {createStore , applyMiddleware}from 'redux';
import {Provider} from 'react-redux';
import firebase from 'firebase';
import reducers from './reducers';
import ReduxThunk from 'redux-thunk';
import LoginForm from './components/LoginForm';
import AppNavigator from './navigators/AppNavigator';




class App extends Component{

  componentWillMount()
  {
    const config = {
      apiKey: 'AIzaSyA8qG1_Enk3mFCKbzOyrNKC6MQN7wMZr6s',
      authDomain: 'manager-9ebb1.firebaseapp.com',
      databaseURL: 'https://manager-9ebb1.firebaseio.com',
      projectId: 'manager-9ebb1',
      storageBucket: 'manager-9ebb1.appspot.com',
      messagingSenderId: '1048901367300'
    };
    firebase.initializeApp(config);
  }

  render()
  {
    const store = createStore(reducers,{},applyMiddleware(ReduxThunk));

    return(
      <Provider store ={store}>
      <AppNavigator />
        </Provider>
    )
  }
}

  
export default App;
