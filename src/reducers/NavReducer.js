import { NavigationActions } from 'react-navigation';
import createReducer from '../../lib/createReducer';
import { AppNavigator } from '../navigators/AppNavigator';

import {
    EMAIL_CHANGED, 
    PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAILED,
    LOGIN_USER,
    LOGOUT
} from '../actions/types';

// const LoginPage = AppNavigator.router.getActionForPathAndParams('Login');
// const tempNavState = AppNavigator.router.getStateForAction(LoginPage);
// const HomePage = AppNavigator.router.getActionForPathAndParams('Main'); 
// const initialNavState = AppNavigator.router.getStateForAction(
// tempNavState
// );

const initialNavState = {
    index: 0,
    routes: [
        { key: 'Login', routeName: 'Login' },
        
    ],
  
};

// function NavReducer(state = initialNavState, action) {
//     let nextState;
//     switch (action.type) {
//       case LOGIN_USER_SUCCESS:
//         nextState = AppNavigator.router.getStateForAction(
//           NavigationActions.navigate({ routeName: 'Main' }),
//           state
//         );
//         break;
//       default:
//         nextState = AppNavigator.router.getStateForAction(action, state);
//         break;
//     }
  
//     // Simply return the original `state` if `nextState` is null or undefined.
//     return nextState || state;
//   }



// const NavReducer = createReducer(initialNavState, {
//     [types.NAVIGATION_NAVIGATE](state, action) {
//         const newState = AppNavigator.router.getStateForAction(action, state);
//         if (action.params) {
//             newState.params = action.params;
//         }
//         return newState;
//     },
    
//     [types.NAVIGATION_BACK](state) {
//         return AppNavigator.router.getStateForAction(NavigationActions.back(), state);
//     }
// });
const NavReducer = (state = initialNavState, action) => {
    const nextState = AppNavigator.router.getStateForAction(action, state);
  
    // Simply return the original `state` if `nextState` is null or undefined.
    return nextState || state;
  };
  
  
export default NavReducer;
