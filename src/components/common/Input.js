import React from 'react';
import {TextInput,View,Text,StyleSheet} from 'react-native';


const Input =({label,value,onChangeText,placeholder,secureTextEntry,keyboardType})=>{

    const{containerStyle,labelStyle,inputStyle}=styles;

    return(
        <View style = {containerStyle}>
            <Text style ={labelStyle}>{label}</Text>
            <TextInput
            keyboardType ={keyboardType}
            secureTextEntry  ={secureTextEntry}
            placeholderTextColor='#ddd'
            autoCapitalize ='none'
            autoCorrect ={true}
            value={value}
            placeholder ={placeholder}
            onChangeText={onChangeText}
            style ={inputStyle}
            />
            </View>
    );

};


const styles = {
    inputStyle:{
        color:'#000',
        paddingRight:5,
        paddingLeft:5,
        fontSize:18,
        lineHeight:23,
        flex:2
    },
    labelStyle:{
        fontSize:18,
        paddingLeft:20,
        flex:1
    },
    containerStyle:{height:40,
    flexDirection:'row',
    flex:1,
alignItems:'center'
}
}
export {Input};