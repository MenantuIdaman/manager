import React, {Component}from'react';
import {View,Picker,Text}from'react-native';
import {Button,Card,CardSection,Input}from './common';
import {connect }from 'react-redux';
import {employeeUpdate} from '../actions/EmployeeActions';


class EmployeeForm extends Component{
    render(){


        return(
            <View>
                <CardSection>
                        <Input
                        label ="Name"
                        placeholder ="John"
                        value={this.props.name}
                        onChangeText ={value=>this.props.employeeUpdate({prop:'name',value})}
                        />
                    </CardSection>
                    <CardSection>
                    <Input
                        label ="Phone"
                        placeholder ="0888-888-8888"
                        keyboardType ='phone-pad'
                        value={this.props.phone}
                        onChangeText ={value=>this.props.employeeUpdate({prop:'phone',value})}
                        />
                        
                    </CardSection>
                    <CardSection style ={{flexDirection:'column'}}>
                        <Text style ={styles.pickerTextStyles}>Shift</Text>
                    <Picker
                    style = {{paddingLeft:30}}
                    selectedValue = {this.props.shift}
                    onValueChange ={value => this.props.employeeUpdate({prop:'shift',value})}
                    >
                        <Picker.Item label= "Monday" value =""/>
                        <Picker.Item label= "Tuesday" value ="Tuesday"/>
                        <Picker.Item label= "Wednesday" value ="Wednesday"/>
                        <Picker.Item label= "Thursday" value ="Thursday"/>
                        <Picker.Item label= "Friday" value ="Friday"/>
                        <Picker.Item label= "Saturday" value ="Saturday"/>
                        <Picker.Item label= "Sunday" value ="Sunday"/>
                        </Picker>
                    </CardSection>
                    </View>
        )
    }
}

const styles = {
    pickerTextStyles:{
        fontSize:18,
        paddingLeft:20
    }
}

const mapStateToProps = (state)=>{
    const{name,phone,shift}=state.employeeForm;
    return{name,phone,shift};
}
export default connect(mapStateToProps,employeeUpdate) (EmployeeForm);