import {EMPLOYEE_UPDATE,EMPLOYEE_CREATE,EMPLOYEE_FETCH_SUCCESS,EMPLOYEE_SAVE_SUCCESS}from './types';
import { NavigationActions} from 'react-navigation';
import firebase from 'firebase';

export const employeeUpdate = ({prop,value})=>{
    return{
        type:EMPLOYEE_UPDATE,
        payload:{prop,value}
    }
}

export const employeeCreate =({name,phone,shift})=>{
  

    const {currentUser} = firebase.auth();

  return(dispatch)=>{

    firebase.database().ref(`/users/${currentUser.uid}/employees`)
    .push({name,phone,shift});

    dispatch({
        type:EMPLOYEE_CREATE
    })
    dispatch(NavigationActions.back());
  }

    

}

export const employeeFetch=()=>{
    const {currentUser} = firebase.auth();

    return(dispatch)=>{
        firebase.database().ref(`/users/${currentUser.uid}/employees`)
        .on('value',snapshot =>{
            dispatch({
                type:EMPLOYEE_FETCH_SUCCESS,
                payload:snapshot.val()
            })
        })
    }
};

export const navigationOnROw =({employee})=>{
   
  return(dispatch)=>{
    const setParamsAction = NavigationActions.setParams({
        params: {employee},
        key: 'EmployeeEdit',
      })
      
      dispatch(NavigationActions.navigate({routeName:setParamsAction.key,params:setParamsAction.params}));
      
      
    }      
}

export const employeeSave = ({name,phone,shift,uid})=>{
    const {currentUser}=firebase.auth();
    const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Main'})
        ]
      })
    

    return (dispatch)=>{
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`)
        .set({name,phone,shift}).then(
            dispatch({type:EMPLOYEE_SAVE_SUCCESS}),
            dispatch(resetAction));
    }

};



export const NavigateAdd =()=>{
     
    return(dispatch)=>{
        dispatch({type:EMPLOYEE_SAVE_SUCCESS}),
        dispatch(NavigationActions.navigate({routeName:'EmployeeCreate'}));
    }
}

export const EmployeeDelete= ({uid})=>{
    const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'Main'})
        ]
      })
    
    const {currentUser}=firebase.auth()
   
    return(dispatch)=>{
        firebase.database().ref(`/users/${currentUser.uid}/employees/${uid}`).remove().then(
            dispatch({type:EMPLOYEE_SAVE_SUCCESS}),
            dispatch(resetAction)
        );
    };
};