import React,{Component}from 'react';
import _ from 'lodash'
import {Card,CardSection,Button,Confirm }from './common';
import EmployeeForm from './EmployeeForm';
import Communications from 'react-native-communications';
import {employeeUpdate,employeeSave,EmployeeDelete} from '../actions/EmployeeActions';
import {connect}from 'react-redux';

class EmpolyeeEdit extends Component{
    state ={
        showModal:false
    }

    componentWillMount(){
       
        _.each(this.props.navigation.state.params.employee,(value,prop)=>{
            this.props.employeeUpdate({prop,value});
        });
        
    }
    onAccept(){
        const{uid}= this.props.navigation.state.params.employee
        this.props.EmployeeDelete({uid});
    }
    onDecline(){
        this.setState({showModal:false})
    }

    onButtonPress(){
        const{name,phone,shift}= this.props;
        this.props.employeeSave({name,phone,shift,uid:this.props.navigation.state.params.employee.uid});
       
    }
    onTextPress(){
        const {name,phone, shift} = this.props;
        Communications.text(phone,`Hey ${name} your upcoming shift is on ${shift}`);
    }

    render(){
         // const{name,phone,shift} = this.props.navigation.state.params.employee;
        // //const {name}= this.props.setActionParams;
        // console.log("props:" + name, phone, shift);
        return(
            <Card>
                <EmployeeForm {...this.props}/>
                <CardSection>
                    <Button
                    onPress={this.onButtonPress.bind(this)}>
                        Save Changes
                        </Button>
                    </CardSection>
                    <CardSection>
                    <Button
                    onPress={this.onTextPress.bind(this)}>
                        Text Employee
                        </Button>
                    </CardSection>

                    <CardSection>
                        <Button
                        onPress ={()=>this.setState({showModal:true})}>
                                Fire Employee
                            </Button>
                        </CardSection>

                    <Confirm
                    visible={this.state.showModal}
                    onAccept={this.onAccept.bind(this)}
                    onDecline={this.onDecline.bind(this)}
                    >
                        Are You sure you want to delete this?
                        </Confirm>

                </Card>
        )
    }

}


const mapStateToProps = (state)=>{
    const{name,phone,shift}=state.employeeForm;
    return{name,phone,shift};
}

export default connect(mapStateToProps,{
    employeeSave,
    employeeUpdate,
    EmployeeDelete 
}) (EmpolyeeEdit);