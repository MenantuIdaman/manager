export const EMAIL_CHANGED = 'email_changed';
export const PASSWORD_CHANGED = 'password_change';
export const LOGIN_USER_SUCCESS = 'login_user_success';
export const LOGIN_USER_FAILED = 'login_user_failed';
export const LOGIN_USER = 'login_user';


export const EMPLOYEE_CREATE = 'employee_create';
export const EMPLOYEE_UPDATE = 'empolyee_update';
export const EMPLOYEE_FETCH_SUCCESS= 'EMPLOYEE_FETCH_SUCCESS';
export const EMPLOYEE_SAVE_SUCCESS = 'EMPLOYEE_SAVE_SUCCESS';