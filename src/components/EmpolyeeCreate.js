import React, {Component}from'react';
import {Button,Card,CardSection}from './common';
import EmployeeForm from './EmployeeForm';
import {connect }from 'react-redux';
import {employeeUpdate,employeeCreate} from '../actions/EmployeeActions';
import INITIAL_STATE from '../reducers/EmployeeFormReducer';
<script src="http://192.168.39.2:8097"></script>

class EmployeeCreate extends Component{


   
constructor(props)
{
    super(props);
    this.state={
        name:'',
        phone:'',
        shift:''||'Monday'
    }
  
}

    onButtonPress()
    { 
        const {name,phone,shift}=this.props;
        // name = this.state;
        // phone = this.state;
        // shift = this.state;
        
        this.props.employeeCreate({name,phone,shift:shift||'Monday'});
    }
    

    render()
    {   
        
        // const{name,phone,shift} = this.props.navigation.state.params.employee;
        // //const {name}= this.props.setActionParams;
        // console.log("props:" + name, phone, shift);
       
        return(
            <Card>
                
                    <EmployeeForm {...this.props} />
                    <CardSection>
                    <Button
                    onPress={this.onButtonPress.bind(this)}
                    >Create</Button>
                    </CardSection>
                </Card>
        )
    }
}

const mapStateToProps = (state)=>{
    const{name,phone,shift}=state.employeeForm;
    return{name,phone,shift};
}



export default connect(mapStateToProps,{
    employeeUpdate,
    employeeCreate
}) (EmployeeCreate);